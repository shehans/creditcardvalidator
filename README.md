# README #

Credit Card number validation service

### How to run the server? ###

* Executable jar file is provided: card-validator.jar OR do a maven build (mvn clean package)
* JRE 11 environment required
* Execute the jar file with following command (from the folder where the jar file is)
* java -jar card-validator.jar

### End points ###

* http://localhost:8080/validate/{cardnumber}
* For example http://localhost:8080/validate/4111111111111111

### Test cases ###

* Test cases are in the source repository src/test/java/cc/validate/service/app/
* Use maven to run the test cases. Maven build will run the test cases. 

### Future enhancements ###

* Use https, JSON Web Token (JWT) for signature and encrypt the card number
* Return JSON response
* Dockerize the project