package cc.validate.service.app.service;

import cc.validate.service.app.enums.CardType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class CardValidator {

    private final Logger logger = LoggerFactory.getLogger(CardValidator.class);

    public String validateCard(String cardNumber) {

        Optional<String> inputError = validateInput(cardNumber);
        if (inputError.isPresent()) {
            return inputError.get();
        }
        String formattedCardNumber = cardNumber.replaceAll("\\s+", "");

        //Step 1 - get card type
        Optional<String> cardTypeOp = getCardType(formattedCardNumber);
        String cardType = cardTypeOp.isEmpty() ? "Unknown" : cardTypeOp.get();

        //Step 2 - validate card number
        boolean isValid = isValidCardNumber(formattedCardNumber);

        return cardType + ": " + cardNumber + (isValid ? " (valid)" : " (invalid)");
    }

    /**
     * Basic input validation
     * @param cardNumber
     * @return error message
     */
    private Optional<String> validateInput(String cardNumber) {
        if (cardNumber == null) {
            return Optional.of("Card number is null");
        }

        String formattedCardNumber = cardNumber.replaceAll("\\s+", "");
        if (!formattedCardNumber.matches("[0-9]+")) {
            return  Optional.of("Card number should contain only numbers");
        }

        return Optional.empty();
    }

    /**
     * This method will return the card type
     * @param cardNumber - Credit card number
     * @return empty for unknown card types
     */
    private Optional<String> getCardType(String cardNumber) {

        for (CardType cardType : CardType.values()) {
            if (cardNumber.matches(cardType.getRegExpression())) {
                return Optional.of(cardType.getLabel());
            }
        }
        return Optional.empty();
    }

    /**
     * Implementation of Luhn Algorithm
     * @param cardNumber
     * @return valid or invalid
     */
    private boolean isValidCardNumber(String cardNumber) {
        //put all the digits to an int array
        int[] digitsArray = new int[cardNumber.length()];
        for (int i = 0 ;i < cardNumber.length(); i++) {
            digitsArray[i] = Integer.parseInt(cardNumber.substring(i, i+1));
        }

        //Starting with the next to last digit and continuing with every other digit
        // going back to the beginning of the card, double the digit.
        for (int j = digitsArray.length - 2; j >= 0; j = j - 2) {
            int digit = digitsArray[j];
            digit = digit * 2;
            //For digits greater than 9 you will need to split them and sum them independently
            if (digit > 9) {
                digit = digit % 10 + 1;
            }
            digitsArray[j] = digit;
        }

        //Sum all doubled and untouched digits in the card number
        int sum = Arrays.stream(digitsArray).sum();

        //If that total is a multiple of 10, the number is valid.
        return sum % 10 == 0;
    }
}
