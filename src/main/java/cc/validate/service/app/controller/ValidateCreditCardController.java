package cc.validate.service.app.controller;

import cc.validate.service.app.service.CardValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/validate")
public class ValidateCreditCardController {

    private final CardValidator cardValidator;

    public ValidateCreditCardController(CardValidator cardValidator) {
        this.cardValidator = cardValidator;
    }

    @GetMapping(path="/{number}")
    public ResponseEntity<String> checkValidCard(@PathVariable String number) {
        try {
            String result = cardValidator.validateCard(number);
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Card validation service failed: " + e.getMessage());
        }
    }
}
