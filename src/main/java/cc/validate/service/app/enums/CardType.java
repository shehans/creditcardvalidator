package cc.validate.service.app.enums;

public enum CardType {

    AMEX("AMEX", "3[47][0-9]{13}"),
    DISCOVER("Discover", "(6011)[0-9]{12}"),
    MASTERCARD("MasterCard", "5[1-5][0-9]{14}"),
    VISA("VISA", "4[0-9]{12}(?:[0-9]{3})?");

    private final String label;
    private final String regExpression;

    CardType(String label, String regExpression) {
        this.label = label;
        this.regExpression = regExpression;
    }

    public String getLabel() {
        return label;
    }

    public String getRegExpression() {
        return regExpression;
    }
}
