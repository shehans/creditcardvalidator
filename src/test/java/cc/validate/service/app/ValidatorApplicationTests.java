package cc.validate.service.app;

import cc.validate.service.app.service.CardValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ValidatorApplicationTests {

	@Autowired
	private CardValidator cardValidator;

	@Test
	void validateCards() {
		assertEquals(cardValidator.validateCard("4111111111111111"),"VISA: 4111111111111111 (valid)");
		assertEquals(cardValidator.validateCard("4111111111111"),"VISA: 4111111111111 (invalid)");
		assertEquals(cardValidator.validateCard("4012888888881881"),"VISA: 4012888888881881 (valid)");
		assertEquals(cardValidator.validateCard("378282246310005"),"AMEX: 378282246310005 (valid)");
		assertEquals(cardValidator.validateCard("6011111111111117"),"Discover: 6011111111111117 (valid)");
		assertEquals(cardValidator.validateCard("5105105105105100"),"MasterCard: 5105105105105100 (valid)");
		assertEquals(cardValidator.validateCard("5105 1051 0510 5106"),"MasterCard: 5105 1051 0510 5106 (invalid)");
		assertEquals(cardValidator.validateCard("9111111111111111"),"Unknown: 9111111111111111 (invalid)");
	}

	@Test
	void cardWithLettersShouldFail() {
		assertTrue(cardValidator.validateCard("4111111b111111a").contains("should contain only numbers"));
	}

	@Test
	void nullCheck() {
		assertTrue(cardValidator.validateCard(null).contains("is null"));
	}

	@Test
	void emptyStringCheck() {
		assertTrue(cardValidator.validateCard("").contains("should contain only numbers"));
	}

}
